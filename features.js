const interval = 500;
const directions = [[0, -1], [-1, 0], [0, 1], [1, 0]]; 
const SIZE = 20;
const initialSnakeStartRow = Math.trunc(SIZE / 2);
const initialSnakeStartCol = 1; 
const initialSnakeSize = Math.trunc(SIZE / 4);
const initialApplePosRow = initialSnakeStartRow;
const initialApplePosCol = SIZE -  initialSnakeSize + 1;
const startArrowCodes = 8592;
var buttons, directionIndex = 3, apple, stopMoving = true, snakeCells = [[1, 1]], timeOutCleared = true,
eatedApples, applesToEat, resumeOrStart;

function move() {
  let rowMove = directions[directionIndex][0];
  let colMove = directions[directionIndex][1];
  let snakeHead = snakeCells[snakeCells.length - 1];
  updateSnake(snakeHead[0] + rowMove, snakeHead[1] + colMove);
  timerID = setTimeout(move, interval);
  if (timeOutCleared) {
    clearTimeout(timerID);
  }
}

var timerID = setTimeout(move, interval);  

function updateSnake(rowIn, colIn) {
  let isSnakeCell = function(row, col) {
    return snakeCells.slice(1).find((cell) => {
      return row === cell[0] && col === cell[1];
    });  
  }
  let newApple = function() {
    let row = 1 + Math.trunc(Math.random() * SIZE);
    let col = 1 + Math.trunc(Math.random() * SIZE);
    while (isSnakeCell(row, col)) {
      ++col;
      if (col > SIZE) {
        col = 1;
        ++row;
        row = row > SIZE ? 1 : row;
      }
    }
    apple = [row, col];
    buttons[row][col].setAttribute('class', 'apple');
  }
  let outcome = function(message) {
    document.getElementById('outcome').innerHTML = message;
    document.getElementById('outcome').innerHTML += '<button onClick="newGame();"> NEW GAME </button>';
    timeOutCleared = true;
    stopMoving = true;
    resumeOrStart = true;
  }
  if (stopMoving) {
    return;
  }
  if ((rowIn >= 1 && rowIn <= SIZE && colIn >= 1 && colIn <= SIZE) && !isSnakeCell(rowIn, colIn)) {
    snakeCells.push([rowIn, colIn]);
    buttons[rowIn][colIn].innerHTML = '&#' + (startArrowCodes + directionIndex) + ';';
    if (rowIn !== apple[0] || colIn !== apple[1]) {
      let pos = snakeCells.shift();
      buttons[pos[0]][pos[1]].setAttribute('class', 'gameSpace');
    } else if (applesToEat === 1) {
      outcome('You won!!!<br>');
    } else {
      document.getElementById("toEat").innerHTML = --applesToEat;
      document.getElementById("eated").innerHTML = ++eatedApples;
      newApple();
    }
    buttons[rowIn][colIn].setAttribute('class', 'snake');
  } else {
    document.getElementById("crash").play();
    buttons[rowIn][colIn].setAttribute('class', 'lose');
    outcome('You lose this game<br>Reason: you steped out of the game space<br>');
  }
}

function newGame() {
  document.getElementById("outcome").innerHTML = '';
  const BORDER_SIZE = SIZE + 2;
  document.getElementById("buttons").innerHTML = '';
  for (let i = 0; i < BORDER_SIZE; ++i) {
    for (let j = 0; j < BORDER_SIZE; ++j) {
      document.getElementById("buttons").innerHTML += '<button id="pos' + i + 'x' + j + '"> v </button>';
    }
    document.getElementById("buttons").innerHTML += '<br>';
  }
  buttons = Array(BORDER_SIZE);
  for (let i = 0; i < BORDER_SIZE; ++i) {
    buttons[i] = Array(BORDER_SIZE);
    for (let j = 0; j < BORDER_SIZE; ++j) {
      buttons[i][j] = document.getElementById("pos" + i + 'x' + j);
      if (i === 0 || i === BORDER_SIZE - 1 || j === 0 || j === BORDER_SIZE - 1) {
        buttons[i][j].setAttribute('class', 'border');
      } else {
        buttons[i][j].setAttribute('class', 'gameSpace');
      }
    }
  }
  apple = [initialApplePosRow, initialApplePosCol];
  buttons[apple[0]][apple[1]].setAttribute('class', 'apple');
  directionIndex = 2;
  snakeCells = [];
  for (let col = 0; col < initialSnakeSize; ++col) {
    let cell = [initialSnakeStartRow, initialSnakeStartCol + col];
    snakeCells.push(cell);
    buttons[cell[0]][cell[1]].setAttribute('class', 'snake');
    buttons[cell[0]][cell[1]].innerHTML = '&#' + (startArrowCodes + directionIndex);
  }
  stopMoving = true;
  document.getElementById("legend").innerHTML = 'Game controls:\
  <ul>\
    <li>To move to the left/right direction press the corresponding arrow key</li>\
    <li>To pause the game press the arrow key whose direction is opposite to the actual movement direction</li>\
    <li>To resume a paused game and move to the left/right direction press the corresponding arrow key</li>\
    <li>To resume a paused game and move in front press any other key </li>\
    <li>Tip: you can also simply pause/resume the game by pressing any key except the arrow keys</li>\
  </ul>';
  eatedApples = 0;
  applesToEat = SIZE * SIZE - initialSnakeSize;
  document.getElementById("outcome").innerHTML = 'You eated <label style="background-color: rgb(0, 255, 76);" id="eated">' + eatedApples + '</label>\
  apples and you have to eat more <label style="background-color: rgb(0, 255, 76);" id="toEat">' + applesToEat + '</label> apples for your valuable cells';
  timeOutCleared = false;
  timerID = setTimeout(move, interval);
}

function startGame() {
  newGame();
  stopMoving = true;
  resumeOrStart = true;
  document.addEventListener('keydown', (event) => {
    stopMoving = false;
    let arrowIndex = ['ArrowLeft', 'ArrowUp', 'ArrowRight', 'ArrowDown'].findIndex((string) => {
      return string === event.key;
    });
    arrowIndex = arrowIndex >= 0 ? arrowIndex : undefined;
    switch ((arrowIndex - directionIndex + 4) % 4) {
      case 3: //left arrow
        document.getElementById("turn").play();
        if (--directionIndex < 0) {
          directionIndex = directions.length - 1;
        }
        resumeOrStart = false;
        break;
      case 1: //right arrow
        document.getElementById("turn").play();
        if (++directionIndex === directions.length) {
          directionIndex = 0;
        }
        resumeOrStart = false;
        break;
      case 0: break;
      default:
        if (resumeOrStart) {
          document.getElementById("turn").play();
          resumeOrStart = false;
        } else {
          stopMoving = true;
          resumeOrStart = true;  
        }
    }
  }) 
}